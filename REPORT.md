# Report 

```
N. C. van Ommeren
University of Amsterdam
10017704
```

## GatorGraphics

This application contains a map of Schiphol and shows which gates are occupied at a certain moment in time. The user is able to select a certain time of the day with a slide bar and the program shows which gates are occupied by drawing a red crosses through it. When the user hoovers over a red cross, the program shows information about the aircraft that is currently active at that particulair gate. Figure 1 shows a printscreen of the program. All the yellow and blue aircrafts on the map are spots where it is possible to park an aircraft, confirm with the business we will call them "VOP" [Vliegtuig Opstel Plaats]. 

![Alt text](https://bytebucket.org/nik1235/gate-graphics/raw/60a190ffbe6ad666a711ef338ad9b3ad242f5b53/doc/printscreen_gatorgraphics.jpg)
Figure 1: Printscreen of Gator Graphics at 14:00, the bottom left corner shows the time.

# Features

- The user can select a different time by clicking on the clock in the toolbar (third button) or by clicking the `edit - time` button in the menu. On click a dialog appears with a slidebar with a default value at 14:00. On the left side it shows the time in minutes after midnight and on the right the time in hours::minutes.

- All occupied vops at a certain time have a red cross through them, when a user hoovers over he sees information about the aircraft in a toolbox. 

- The user can save coordinates to a file. He can do this by clicking the XY button in the toolbar or the `edit - coordinates` in the menu. On click a dialog appears where the user is able to type the name of the VopGroup. This is saved in a stringstream together with all the coordinates of the points the user clicks. To show the user he has succesfully clicked a coordinate, the program shows a purple cross. When the user clicks the edit coordinate button again, the stringstream is written to the file output.txt. There is a maximum on the number of buttons a user can click to make sure the memory is not overloaded. The format of the output makes it easy for the user to copy this to the `Coordinates.txt` input file. I did not want to do this automatically as it is not often needed to change the coordinates and to protect the user against himself.

- When the user hoovers over an occupied VOP it shows a tooltip with information about the aircraft that is currently at the VOP. All information is saved in the Aircraft object and with the tooltip class the box is drawn to the screen. It uses a Gradient class to make the box transparent. All aircrafts that are active at a certain time are saved in a ActiveAircraft vector.

## Final Structure

The following picture describes the structure of the application. 
![Alt text](https://bytebucket.org/nik1235/gate-graphics/raw/60a190ffbe6ad666a711ef338ad9b3ad242f5b53/doc/final_structure.jpg)
Picture 2: Structure of the application.

Compared to the original design document, the `Occupied` class is removed, because the relationship with an aircraft is not 1 - n, but 1 - 1, which made this class redundant. The gator_graph class is renamed to Airport to make it more intuitive and make a clear distinction with the class that contains the whole app which is called `GatorGraphics`. Because I had almost no experience with the structure of a MFC program, I did not know upfront which classes I needed. Every Dialog has his own class, user's can change the values and this is received in the view. The GatorGraph View does the drawing of the background, tooltips and the crosses to the screen. 

The program is divided in three subfolders, the view contains all the dialogs, the view and the mainframe. In addition it contains an extra folder `tooltip` which contains all files to draw a tooltip to the screen. They are in a seperate file as they are from a different source. I did adapt them to make them suitable with this application and compatible with C++11. The domain folder contains the application itself, the document and the objects. The utl folder contains a my_time.h with 2 functions, these are functions that could be usefull for other applications. 

## Challenges

- How to develop an application in MFC. Found a book and made two sample applications, this really helped in developing my own application.
- The coordinate system of the screen. Because I use scrollview the coordinates the user sees are different from the coordinates seen by the computer. Solved the problem by converting the coordinates with help of Visual C++.
- The implementation of the TFXDataTooltip class: They are written in Visual Studio 2010 and it was not clear which other sources it used. It also had his own precompiled header file. I checked which functions caused problems, adapted or removed them to make it work for this project.
- The background is now redrawn every time there is an update in the view. Ideally it would only be drawn once, which I did  by implementing an OnEraseBackground in the Mainframe class. Unfortunatly the scroll and zoom functions did not work anymore, so I chose to go back to the bitblt solution. 