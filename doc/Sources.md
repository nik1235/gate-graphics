Sources:
=======

This file lists the different sources used in this project:

# Books

- Horton, I. (2011). Ivor Horton's Beginning Visual C++ 2008. John Wiley & Sons.
- Lippman, S. B., Lajoie, J., & Moo, B. E. (2005). C++ Primer. Addison-Wesley.

# Tutorials

- http://www.codeproject.com/Articles/1858/Statusbar-it-s-just-so-easy-to-use-in-MFC
- http://www.codeproject.com/Articles/20202/The-Ultimate-Toolbox-Graphical-User-Interface-Clas
- http://depts.washington.edu/cmmr/biga/chapter_tutorials/1.C++_MFC_D3DOGL/1.StepByStepGuide/tutorial_1.html

# References

- http://www.cplusplus.com/reference/
- https://msdn.microsoft.com/en-us/library/dd831853.aspx 
- http://stackoverflow.com/