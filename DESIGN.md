# Design Document

The classes and function related to the controller are linked as follows:

![Alt text](https://bytebucket.org/nik1235/gate-graphics/raw/30cf48b469720fc780e63ab2369543d089c9f278/doc/structure_controller.jpg)

The input files are space seperated `.txt` files. The data is processed in C++. The `GatorGraph` class contains all necessary information to generate the view of this application. The visualisation is completed with the Microsoft Foundation Class. The classes and functionality related to the view are the following:

![Alt text](https://bytebucket.org/nik1235/gate-graphics/raw/30cf48b469720fc780e63ab2369543d089c9f278/doc/structure_view.jpg)

When the user asks for a view of a different moment in time, the value of the time slidebar changes. This is sent to the Gator Graph Class. This activates the Gator Graph Printer to either add aircraft information for active vops or to draw a X for an inactive vop. The Gator Graph Printer also draws the map and the Time Slidebar.