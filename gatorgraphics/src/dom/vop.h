#pragma once

#include "aircraft.h"

#include <map>
#include <string>
#include <vector>

	
/**
*
* A Vop is a parking spot on the map.
* 
* A Vop (Verkeers Opslag Plek - Traffic Storage Spot) is spot where an aircraft is able to park.
* This can either be on a gate or a buffer spot. Every Vop has a list of aircrafts which park on this
* Vop on a certain time of the day. In addition it has a category number, which determines 
* the maximum size of the aircraft.
*
*/
class Vop {
public:
	typedef std::map<std::string, Vop> Vops;
	
	Vop(const std::string& name, const int category,  const char type, 
		const bool is_active, const bool is_reserve) : name_(name), 
		category_(category), type_(type), is_active_(is_active), is_reserve_(is_reserve),
		x_c_(0), y_c_(0) { }

	void addAircraft(const Aircraft& aircraft) { this->aircrafts_.insert(std::pair<int, Aircraft>(aircraft.end_time(), aircraft)); }

	void setCoordinates(const int& x, const int& y) { this->x_c_ = x, this->y_c_ = y; }

	const std::string& name() const { return this->name_; }

	const Aircraft::VopSchedule& aircrafts() const { return this->aircrafts_;  }

	const int& category() const { return this->category_;  }

	const int& x_c() const { return this->x_c_;  }
	
	const int& y_c() const { return this->y_c_; }

private:
	const std::string name_;
	const int category_;				// Size of the aircraft
	Aircraft::VopSchedule aircrafts_;	// Aircrafts at this vop
	int x_c_;
	int y_c_;
	const bool is_active_;				// True when a vop is used this day
	const char type_;					// Buffer (B), Wissel (W), Flexible (F), Shengen (S),
										// Non-Shengen (N) 
	const bool is_reserve_;				// Status equals R	
};
