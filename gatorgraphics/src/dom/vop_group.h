#ifndef VOP_GROUP_H
#define VOP_GROUP_H

#include "vop.h"

#include <map>
#include <string>



/**
*
* A VopGroup is a collection of Vops. 
*
* Every Vop Group has a name, also called the area and contains Vops. 
*
*/
class VopGroup {		
	public:
		typedef std::map<const std::string, VopGroup> VopGroups;

		// Constructor for a Vop Group
		VopGroup(const std::string& name, const Vop::Vops& vops) : name_(name), vops_(vops) { }
	
		// Add Vop to VopGroup
		void addVop(const Vop& vop) { this->vops_.insert(std::pair<std::string, Vop>(vop.name(), vop)); }

		// Find a Vop in a VopGroup
		const Vop& findVop(const std::string& vop_name) const;

		// Add coordinates to a Vop
		void addVopCoordinates(const std::string& vop_name, const int& x_c_, const int& y_c_);
		
		// Add an aircraft to a Vop Group
		void addAircraft(const Aircraft& aircraft, const std::string& vop);

		const std::string& name() const { return this->name_;  }

		const Vop::Vops& vops() const { return this->vops_; }

	private:
		const std::string name_;
		Vop::Vops vops_;
};

#endif