#include "vop_group.h"
#include "vop.h"

#include <sstream>


const Vop& VopGroup::findVop(const std::string& vop_name) const {

	if (this->vops().find(vop_name) != this->vops().end()) {
		return this->vops().find(vop_name)->second;
	}
	else {
		// Throw error when vop is not found
		std::stringstream error_message;
		error_message << "The vop: " << vop_name << " can not be found in the vop group: " <<
			this->name() << "." << std::endl;
		throw std::runtime_error(error_message.str());
	}
}

void VopGroup::addVopCoordinates(const std::string& vop_name, const int& x_c, const int& y_c) {
	if (this->vops().find(vop_name) != this->vops().end()) {
		this->vops_.find(vop_name)->second.setCoordinates(x_c, y_c);
	
	}
	else {
		// Throw error when vop is not found
		std::stringstream error_message;
		error_message << "The vop: " << vop_name << " can not be found when trying to add coordinates." 
			<< std::endl;
		throw std::runtime_error(error_message.str());
	}
}

void VopGroup::addAircraft(const Aircraft& aircraft, const std::string& vop) {
	if (this->vops_.find(vop) != this->vops_.end()) {
		this->vops_.find(vop)->second.addAircraft(aircraft);
	}
}