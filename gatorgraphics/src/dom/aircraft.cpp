#include <fstream>
#include <iostream>

#include "aircraft.h"
#include "../utl/my_time.h"


std::string Aircraft::getAircraftInfo() const {
	
	std::string info = "Type: " + this->type() + "\t";
	info += "Category: " + std::to_string(this->category()) + "\n\n";
	info += "\t\tArriving\t\tDeparting\n";
	info += "Flightnumber:\t" + this->arr_flight() + " \t";
	if (this->arr_flight() == ".")
		info += "\t";
	info += this->dep_flight() + "\n";
	info += "Station:\t" + this->arr_station() + "\t\t" + this->dep_station() + "\n";
	info += "City:\t\t" + this->arr_country() + "\t\t" + this->dep_country() + "\n";
	info += "Schengen:\t"; info += this->arr_schengen(); info += "\t\t";
	info += this->dep_schengen(); info += "\n";
	info += "Reserved Time:\t" + minutes_2_timestring(this->start_time()) + "\t\t";
	info += minutes_2_timestring(this->end_time());
	
	return info;
}