
// GatorgraphicsDoc.h : interface of the CGatorgraphicsDoc class
//


#pragma once

#include "airport.h"

#include <memory>

#include <boost/optional.hpp>

class CGatorgraphicsDoc : public CDocument
{
protected: // create from serialization only
	CGatorgraphicsDoc();
	DECLARE_DYNCREATE(CGatorgraphicsDoc)

// Attributes
public:

// Operations
public:
	CSize GetDocSize() const { return m_DocSize; }						// Retrieve the document size 
	const VopGroup::VopGroups& GetAllVops() const { return m_AllVops; }	// Retrieve all vops 

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// Implementation
public:
	virtual ~CGatorgraphicsDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CSize m_DocSize{ CSize {2300,1800} };

private:
	VopGroup::VopGroups m_AllVops;
	Airport m_Airport{ m_AllVops };

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// Helper function that sets search content for a Search Handler
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
public:
	virtual void OnCloseDocument();
};
