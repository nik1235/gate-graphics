#pragma once

#include <map>
#include <memory>
#include <string>
#include <vector>

/**
*
* An Aircraft occupies a Vop for a certain period. 
* 
* An aircraft stands on a VOP for a certain time. The reserved time for this aircraft is
* saved in the start and end time. The aircraft arrives from a station and country, this is saved
* in the arr_station and arr_country and the aircraft had flightnumber: arr_flight. The aircraft 
* departs to a station and country, which is saved in dep_station and dep_country. The size and type
* aircraft is saved in category and type. Some flights arrive and depart from and to a Schengen country, 
this is saved in dep_schengen. 
*/
class Aircraft {
public:
	typedef std::vector<Aircraft&> Aircrafts;

	typedef std::map<int, Aircraft> VopSchedule;

	// Constructor for an aircraft
	Aircraft(const std::string& arr_flight, const std::string& arr_station, 
		const std::string& arr_country, const char& arr_schengen, const std::string& dep_flight,
		const std::string& dep_station, const std::string& dep_country, const char& dep_schengen,
		const int& category, const std::string& type, const int& start_time, const int& end_time ) :
		arr_flight_(arr_flight), arr_station_(arr_station), arr_country_(arr_country), arr_schengen_(arr_schengen),
		dep_flight_(dep_flight), dep_station_(dep_station), dep_country_(dep_country), dep_schengen_(dep_schengen),
		category_(category), type_(type), start_time_(start_time), end_time_(end_time) { }

	// Get the information about one aircraft in a string, to show in a tooltip in the view
	std::string getAircraftInfo() const;

	const std::string& arr_flight() const { return this->arr_flight_; }

	const std::string& arr_station() const { return this->arr_station_; }

	const std::string& arr_country() const { return this->arr_country_; }

	const char& arr_schengen() const { return this->arr_schengen_; }

	const std::string& dep_flight() const { return this->dep_flight_; }

	const std::string& dep_station() const { return this->dep_station_; }

	const std::string& dep_country() const { return this->dep_country_; }

	const char& dep_schengen() const { return this->dep_schengen_; }

	const int& category() const { return this->category_; }

	const std::string& type() const { return this->type_; }

	const int& start_time() const { return this->start_time_; }

	const int& end_time() const { return this->end_time_; }

private:
	const std::string arr_flight_;
	const std::string arr_station_;
	const std::string arr_country_;
	const char arr_schengen_;
	const std::string dep_flight_;
	const std::string dep_station_;
	const std::string dep_country_;
	const char dep_schengen_;
	const int category_;
	const std::string type_;
	const int start_time_;
	const int end_time_;
}; 
