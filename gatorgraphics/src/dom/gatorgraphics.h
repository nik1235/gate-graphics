
// Gatorgraphics.h : main header file for the gatorgraphics application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "../view/resource.h"       // main symbols


// CGatorgraphicsApp:
// See Gatorgraphics.cpp for the implementation of this class
//

class CGatorgraphicsApp : public CWinApp
{
public:
	CGatorgraphicsApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CGatorgraphicsApp theApp;
