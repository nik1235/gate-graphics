
// GatorgraphicsDoc.cpp : implementation of the CGatorgraphicsDoc class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "Gatorgraphics.h"
#endif

#include "GatorgraphicsDoc.h"
#include "airport.h"

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>

#include <iostream>

#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CGatorgraphicsDoc

IMPLEMENT_DYNCREATE(CGatorgraphicsDoc, CDocument)

BEGIN_MESSAGE_MAP(CGatorgraphicsDoc, CDocument)
END_MESSAGE_MAP()

// CGatorgraphicsDoc construction/destruction

CGatorgraphicsDoc::CGatorgraphicsDoc()
{
}

CGatorgraphicsDoc::~CGatorgraphicsDoc()
{
}

BOOL CGatorgraphicsDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// Create objects
	try {
		
		// Input files
		const std::string vop_file = "dat/inp/Vops_input.txt";
		const std::string vop_skipped_file = "dat/inp/Vops_skipped.txt";
		const std::string solution_file = "dat/inp/FinalSolution.txt";
		const std::string coordinate_file = "dat/inp/Coordinates.txt";

		m_Airport.createObjects(vop_file, vop_skipped_file, solution_file, coordinate_file);
	}
	catch (std::exception&e)  {
		std::cout << e.what();
		BOOST_LOG_TRIVIAL(fatal) << e.what() << "\n";
		return FALSE;
	}
	catch (...) {
		std::cout << "Exception of unknown type!\\n";
		BOOST_LOG_TRIVIAL(fatal) << "Exception of unknown type!\\n";
		return FALSE;
	}

	return TRUE;
}

// CGatorgraphicsDoc serialization

void CGatorgraphicsDoc::Serialize(CArchive& ar)
{
}

#ifdef SHARED_HANDLERS

// Support for thumbnails
void CGatorgraphicsDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// Modify this code to draw the document's data
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// Support for Search Handlers
void CGatorgraphicsDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// Set search contents from document's data. 
	// The content parts should be separated by ";"

	// For example:  strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CGatorgraphicsDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CGatorgraphicsDoc diagnostics

#ifdef _DEBUG
void CGatorgraphicsDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGatorgraphicsDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CGatorgraphicsDoc commands


void CGatorgraphicsDoc::OnCloseDocument()
{

	CDocument::OnCloseDocument();
}

