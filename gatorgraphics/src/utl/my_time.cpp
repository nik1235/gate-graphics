#include "my_time.h"

#include <sstream>
#include <assert.h>

unsigned int timestring_2_minutes(const std::string& time) {

	assert(time.size() == 5);

	int minutes, hours;
	char colon;
	std::istringstream(time) >> hours >> colon >> minutes;
	// int hours = (int)minutes / 100;
	minutes = hours * 60 + minutes;

	if (minutes > 1440 || minutes < 0) {
		std::stringstream error_message;
		error_message << "Time string: " << time <<
			" has incorrect value (expected range from 0000 to 2359)";
		throw std::invalid_argument(error_message.str());
	}
	return minutes;
}

std::string minutes_2_timestring(unsigned int minutes_after_midnight) {

	std::string time;

	assert(minutes_after_midnight >= 0);

	unsigned int hour = minutes_after_midnight / 60;
	if (hour < 10) {
		time = "0" + std::to_string(hour) + ":";
	}
	else {
		time = std::to_string(hour) + ":";
	}

	unsigned int minutes = minutes_after_midnight - hour * 60;
	if (minutes < 10) {
		time += "0" + std::to_string(minutes);
	}
	else {
		time += std::to_string(minutes);
	}

	return time;
}

