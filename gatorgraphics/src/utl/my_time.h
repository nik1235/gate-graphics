#pragma once

#include <string>

/**
* Converts a time string ("00:04") to minutes (4)
*/
unsigned int timestring_2_minutes(const std::string& time);

/**
* Converts minutes (120) to a time string ("02:00")
*/
std::string minutes_2_timestring(unsigned int minutes_after_midnight);

