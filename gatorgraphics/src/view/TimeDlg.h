#pragma once
#include "afxcmn.h"


// CTimeDlg dialog

class CTimeDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CTimeDlg)

public:
	CTimeDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTimeDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TIME_DLG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
private:
	CSliderCtrl m_HSliderTimeCtr;
public:
	virtual BOOL OnInitDialog();
public:
	CString m_HSliderTime;		// Time in minutes
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
private:
	CString m_TimeString;		// Time in hours::minutes
};
