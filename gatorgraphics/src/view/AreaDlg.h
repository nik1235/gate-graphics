#pragma once

#include <string>

// CAreaDlg dialog

class CAreaDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CAreaDlg)

public:
	CAreaDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAreaDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_AREA_DLG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	// The name of the area given by the user
	CString m_Area;
	virtual BOOL OnInitDialog();
};
