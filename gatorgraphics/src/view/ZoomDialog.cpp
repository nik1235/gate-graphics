// ZoomDialog.cpp : implementation file
//

#include "../dom/stdafx.h"
#include "../dom/Gatorgraphics.h"
#include "ZoomDialog.h"
#include "afxdialogex.h"


// CZoomDialog dialog

IMPLEMENT_DYNAMIC(CZoomDialog, CDialogEx)

CZoomDialog::CZoomDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(ID_ZOOM_DLG, pParent)
	, m_Scale(0)
{

}

CZoomDialog::~CZoomDialog()
{
}

void CZoomDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_ZOOM, m_Scale);
	DDV_MinMaxInt(pDX, m_Scale, 1, 4);
	DDX_Control(pDX, IDC_SPIN_ZOOM, m_Spin);
}


BEGIN_MESSAGE_MAP(CZoomDialog, CDialogEx)
END_MESSAGE_MAP()


// CZoomDialog message handlers


BOOL CZoomDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_Spin.SetRange(1, 4);			// Set the spin control range

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}
