
// MainFrm.h : interface of the CMainFrame class
//
#pragma once

#define WS_EX_COMPOSITED 0x02000000L

#include <string>

class CMainFrame : public CFrameWnd
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	std::string m_statusBarTime{ "14:00" };

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar        m_wndStatusBar;
	CToolBar          m_wndToolBar;


// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnUpdateTimeIndicator(CCmdUI *pCmdUI);
	DECLARE_MESSAGE_MAP()
};


