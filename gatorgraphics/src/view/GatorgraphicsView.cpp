
// GatorgraphicsView.cpp : implementation of the CGatorgraphicsView class
//
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.

#include "../dom/stdafx.h"
#ifndef SHARED_HANDLERS
#include "../dom/Gatorgraphics.h"
#endif
#include "../dom/GatorgraphicsDoc.h"
#include "tooltip/tfxdatatip.h"
#include "../utl/my_time.h"

#include "AreaDlg.h"
#include "GatorgraphicsView.h"
#include "MainFrm.h"
#include "TimeDlg.h"
#include "ZoomDialog.h"

#include <iostream>
#include <fstream>
#include <string>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CGatorgraphicsView

IMPLEMENT_DYNCREATE(CGatorgraphicsView, CScrollView)

BEGIN_MESSAGE_MAP(CGatorgraphicsView, CScrollView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_COMMAND(ID_VIEW_ZOOM, &CGatorgraphicsView::OnViewZoom)
	ON_WM_LBUTTONDOWN()
	ON_COMMAND(ID_EDIT_COORDINATES, &CGatorgraphicsView::OnEditCoordinates)
	ON_WM_VSCROLL()
	ON_COMMAND(ID_EDIT_TIME, &CGatorgraphicsView::OnEditTime)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COORDINATES, &CGatorgraphicsView::OnUpdateEditCoordinates)
	ON_WM_MOUSEMOVE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CGatorgraphicsView construction/destruction

CGatorgraphicsView::CGatorgraphicsView()
{
	SetScrollSizes(MM_TEXT, CSize{});	// Set arbitrary scrollers

}

CGatorgraphicsView::~CGatorgraphicsView()
{
}

BOOL CGatorgraphicsView::PreCreateWindow(CREATESTRUCT& cs)
{
	// Create a tooltip to show the information about an aircraft
	m_AircraftInfo.Create(this, 95L, true);
	m_AircraftInfo.On(true);
	m_AircraftInfo.SetSurrounding(100, 100);
	m_AircraftInfo.SetMSecsToHide(1000 * 30);

	return CScrollView::PreCreateWindow(cs);
}

// CGatorgraphicsView drawing

void CGatorgraphicsView::OnDraw(CDC* pDC)
{
	CGatorgraphicsDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	drawBackground(pDC);

	drawVops(pDC);
}

void CGatorgraphicsView::drawBackground(CDC* pDC)
{
	CImage image;
	image.Load(_T("res/Plaat21_v2.bmp")); 
	
	// Error handling
	if (image.IsNull()) 
		MessageBox(L"Unable to Load the background Image", 0, MB_ICONERROR | MB_OK);
	else {
		int height = image.GetHeight();
		int width = image.GetWidth();

		HDC ffMemoryDc2 = ::CreateCompatibleDC(*pDC);

		// Draw bitmap to screen
		HBITMAP hOldBitmap2 = (HBITMAP) ::SelectObject(ffMemoryDc2, image);
		BitBlt(*pDC, 0, 0, width, height, ffMemoryDc2, 0, 0, SRCCOPY); 	
	}
}

void CGatorgraphicsView::drawVops(CDC* pDC) {

	CGatorgraphicsDoc* pDoc{ GetDocument() };
	const VopGroup::VopGroups& all_vops = pDoc->GetAllVops();

	// Set text color to black
	pDC->SetTextColor(RGB(0, 0, 0));

	// Clear the vector with previous active aircrafts
	if (m_bChangedTime)
		m_ActiveAircrafts.clear();

	for (auto vop_group : all_vops) {
		for (auto vop : vop_group.second.vops()) {
			// Update list with active aircrafts if the time is changed
			if (m_bChangedTime) 
				determineActiveAircraft(pDC, vop.second);

			// Print name and category of the vop to the screen when coordinates are found
			if (vop.second.x_c() != 0 || vop.second.y_c() != 0) {
				CString vop_name, category;
				vop_name = vop.first.c_str();
				pDC->TextOut(vop.second.x_c(), vop.second.y_c() - 30, vop_name);
				category.Format(_T("%d"), vop.second.category());
				pDC->TextOut(vop.second.x_c(), vop.second.y_c() - 60, category);
			}
		}
	}

	// Declare a red solid pen for drawing the crosses
	CPen aRedPen;
	aRedPen.CreatePen(PS_SOLID, 6, RGB(255, 0, 0));
	CPen* pOldPen = pDC->SelectObject(&aRedPen);

	// Loop over the list of active aircrafts and draw crosses
	for (auto coordinates : m_ActiveAircrafts) {
		drawCross(pDC, coordinates.second.x, coordinates.second.y);
	}

	m_bChangedTime = false;
	// Set the color back to the default value
	pDC->SelectObject(pOldPen); 
}

void CGatorgraphicsView::drawCross(CDC* pDC, int x_c, int y_c)
{
	// Draw lines forming a X
	pDC->MoveTo(x_c, y_c);
	pDC->LineTo(x_c + m_VopSize, y_c + m_VopSize);
	pDC->MoveTo(x_c, y_c + m_VopSize);
	pDC->LineTo(x_c + m_VopSize, y_c);
}

void CGatorgraphicsView::determineActiveAircraft(CDC* pDC, const Vop& vop) {

	for (auto aircraft : vop.aircrafts()) {
		
		// Check if aircraft is active at the chosen time
		if (aircraft.first >= m_SliderTime && aircraft.second.start_time() <= m_SliderTime) {
			
			// Only draw crosses for aircraft you know the coordinates of
			if (vop.x_c() != 0 && vop.y_c() != 0) {
				
				// Add aircraft to the list of active aircrafts								
				CPoint vop_coordinates(vop.x_c(), vop.y_c());
				ActiveAircraft active_aircraft(aircraft.second, vop_coordinates);
				m_ActiveAircrafts.push_back(active_aircraft);
			}
		}
	}
}

// CGatorgraphicsView diagnostics
#ifdef _DEBUG
void CGatorgraphicsView::AssertValid() const
{
	CView::AssertValid();
}

void CGatorgraphicsView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CGatorgraphicsDoc* CGatorgraphicsView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGatorgraphicsDoc)));
	return (CGatorgraphicsDoc*)m_pDocument;
}
#endif //_DEBUG


// CGatorgraphicsView message handlers


void CGatorgraphicsView::OnInitialUpdate()
{
	
	ResetScrollSizes();				// Set up the scrollbars

	CScrollView::OnInitialUpdate();
}


void CGatorgraphicsView::OnViewZoom()
{
	// Pass the scale to the dialog object
	CZoomDialog aDlg;				
	aDlg.m_Scale = m_Scale;		

	// When the user clicks the OK button
	if (aDlg.DoModal() == IDOK) {
		
		// Get the new scale
		m_Scale = aDlg.m_Scale;		
		ResetScrollSizes();
		
		// Invalidate the whole window
		InvalidateRect(nullptr);	
	}
}

void CGatorgraphicsView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo)
{

	CScrollView::OnPrepareDC(pDC, pInfo);
	CGatorgraphicsDoc* pDoc{ GetDocument() };
	pDC->SetMapMode(MM_ANISOTROPIC);              // Set the map mode
	CSize DocSize{ pDoc->GetDocSize() };          // Get the document size
	pDC->SetWindowExt(DocSize);                   // Now set the window extent

	// Get the number of pixels per inch in x and y											  
	int xLogPixels{ pDC->GetDeviceCaps(LOGPIXELSX) };
	int yLogPixels{ pDC->GetDeviceCaps(LOGPIXELSY) };

	// Calculate the viewport extent in x and y for the current scale
	int xExtent{ (DocSize.cx*m_Scale*xLogPixels) / 250 };
	int yExtent{ (DocSize.cy*m_Scale*yLogPixels) / 250 };

	pDC->SetViewportExt(xExtent, yExtent);        // Set viewport extent
}

// Source: Visual C++
void CGatorgraphicsView::ResetScrollSizes()
{
	CClientDC aDC{ this };
	
	// Get the size of the document in pixels
	OnPrepareDC(&aDC);                              
	CSize DocSize{ GetDocument()->GetDocSize() };  	
	aDC.LPtoDP(&DocSize);   

	// Set up the scrollbars
	SetScrollSizes(MM_TEXT, DocSize);              
}


void CGatorgraphicsView::OnLButtonDown(UINT nFlags, CPoint point)
{
	CClientDC aDC{ this };      

	// Adjust the origin and convert point to logical coordinates
	OnPrepareDC(&aDC);                                      
	aDC.DPtoLP(&point);                                     

	// If the program is in Coordinate mode
	if (m_EditCoordinates) {

		// Create a purple solid pen
		CPen aPurplePen;
		aPurplePen.CreatePen(PS_SOLID, 6, RGB(148, 0, 211));
		CPen* pOldPen = aDC.SelectObject(&aPurplePen);

		// Save the position of the cursor
		m_SelectedPoint = point;         

		// Draw purple crosses to show the user he has clicked
		int top_corner = round(0.5 * m_VopSize);
		drawCross(&aDC, point.x - top_corner, point.y - top_corner);

		// Save Coordinates in a string
		m_Coordinates	<< m_CoordinateArea[1] << "\t" 
						<< point.x - top_corner << "\t"<< point.y - top_corner << "\n"; 

		// Increment number of saved coordinates
		m_NrOfCoordinates++;

		// Check that the number of coordinates saved in memory is not too large
		if (m_NrOfCoordinates > 40) {
			MessageBox(L"Reached Maximum number of Coordinates.", 0, MB_ICONERROR | MB_OK);
			m_Coordinates.clear();
			m_NrOfCoordinates = 0;
			// Invalidate the whole window
			InvalidateRect(nullptr);
		}

		aDC.SelectObject(&pOldPen);
	}
	CScrollView::OnLButtonDown(nFlags, point);
}


void CGatorgraphicsView::OnEditCoordinates()
{
	if (m_EditCoordinates && m_NrOfCoordinates != 0) {
		// Write Coordinates to file
		std::ofstream myfile;
		myfile.open("./dat/out/output.txt");
		myfile	<< "This file contains the generated coordinates in the coordinate mode of the program\n"
				<< "==\t" << m_CoordinateArea << "\n"
				<< m_Coordinates.rdbuf();
		myfile.close();

		m_NrOfCoordinates = 0;								// Reset number of saved coordinates
		MessageBox(L"The Coordinates are Succesfully Saved in the Output.txt.",
			0, MB_ICONINFORMATION | MB_OK);

		InvalidateRect(nullptr);							// Invalidate the whole window
	}
	else {
		CAreaDlg areaDlg;

		areaDlg.m_Area = "";								// Pass the view scale to the dialog
		if (areaDlg.DoModal() == IDOK) {
			
			CT2CA pszConvertedAnsiString(areaDlg.m_Area);	// Convert a TCHAR string to a LPCSTR			
			std::string temp(pszConvertedAnsiString);		// Construct a std::string using the LPCSTR input
			if (temp.length() < 2)
				m_CoordinateArea = "AA";
			else
				m_CoordinateArea = temp;
			InvalidateRect(nullptr);						// Invalidate the whole window
		}
	}

	m_EditCoordinates = !m_EditCoordinates;
}


void CGatorgraphicsView::OnEditTime()
{
	CTimeDlg timeDlg;
	// Pass the view time to the dialog
	timeDlg.m_HSliderTime.Format(L"%d", m_SliderTime);	
	
	// When the user clicks the OK button
	if (timeDlg.DoModal() == IDOK) {
		// Get the new time
		m_SliderTime = _wtoi(timeDlg.m_HSliderTime.GetString());	
		InvalidateRect(nullptr);		
		// Change time in the status bar
		CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
		pFrame->m_statusBarTime = minutes_2_timestring(m_SliderTime);

	}
	m_bChangedTime = true;

}


void CGatorgraphicsView::OnUpdateEditCoordinates(CCmdUI *pCmdUI)
{
	// Set button checked when program is in edit coordinate mode
	pCmdUI->SetCheck(m_EditCoordinates);

}


void CGatorgraphicsView::OnMouseMove(UINT nFlags, CPoint point)
{
	CClientDC aDC(this);

	// Adjust the origin and convert point to logical coordinates
	OnPrepareDC(&aDC);							
	aDC.DPtoLP(&point);                         

	// Print information about an active aircraft in a data tip
	if (boost::optional<ActiveAircraft> aircraft = FindOccupiedVop(point)) {
		ActiveAircraft found_aircraft = *aircraft;

		std::string info = (*aircraft).first.getAircraftInfo();
		m_AircraftInfo.SetDataTip(info.c_str(), RGB(204, 255, 255), true);
	}
}

boost::optional<CGatorgraphicsView::ActiveAircraft> CGatorgraphicsView::FindOccupiedVop(const CPoint& point) {

	for (auto active_aircraft : this->m_ActiveAircrafts) {
		// Rectangle around a vop 
		CRect rect(active_aircraft.second.x, active_aircraft.second.y, 
			active_aircraft.second.x + m_VopSize, active_aircraft.second.y + m_VopSize);

		// Check whether cursor is on a vop
		if (rect.PtInRect(point)) {
			return active_aircraft;
		}
	}
	return boost::optional<ActiveAircraft>();
}
