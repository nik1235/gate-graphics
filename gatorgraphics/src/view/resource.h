//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by gatorgraphics.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_gatorgraphicsTYPE           130
#define ID_ZOOM_DLG                     310
#define IDD_TIME_DLG                    315
#define IDD_AREA_DLG                    317
#define IDC_SPIN_ZOOM                   1000
#define IDC_ZOOM                        1001
#define IDC_VIEW_SCALE                  1002
#define IDC_V_SLIDER_TIME_CTR           1003
#define IDC_V_SLIDER_TIME               1004
#define IDC_TIME_STRING                 1008
#define IDC_EDIT1                       1011
#define IDC_EDIT_AREA                   1011
#define ID_VIEW_SCALE                   32771
#define ID_VIEW_ZOOM                    32772
#define ID_BUTTON32773                  32773
#define ID_EDIT_COORDINATES             32777
#define ID_TIME                         32779
#define ID_EDIT_TIME                    32780
#define IDR_TOOLBAR                     59397

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        322
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
