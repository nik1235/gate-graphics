
// GatorgraphicsView.h : interface of the CGatorgraphicsView class
//

#pragma once

#include <sstream>

#include "tooltip/tfxdatatip.h"


class CGatorgraphicsView : public CScrollView
{
	typedef std::pair<Aircraft, CPoint> ActiveAircraft;
	typedef std::vector<ActiveAircraft> ActiveAircrafts;

protected: 
	CGatorgraphicsView();
	DECLARE_DYNCREATE(CGatorgraphicsView)

// Attributes
public:
	CGatorgraphicsDoc* GetDocument() const;

// Overrides
public:
	virtual void OnDraw(CDC* pDC); 
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);

// Implementation
public:
	virtual ~CGatorgraphicsView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
public:
	afx_msg void OnViewZoom();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnEditCoordinates();
	afx_msg void OnEditTime();
	afx_msg void OnUpdateEditCoordinates(CCmdUI *pCmdUI);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

protected:
	void drawBackground(CDC* pDC);
	void drawVops(CDC* pDC);
	void drawCross(CDC* pDC, int x_c, int y_c);
	void determineActiveAircraft(CDC* pDC, const Vop& vop);
	boost::optional<ActiveAircraft> FindOccupiedVop(const CPoint& point);
	void ResetScrollSizes();

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

private:
	// Members of GatorGraphicsView
	const int		m_VopSize{ 34 };			// Size (inches) of a vop on the screen
	int				m_Scale{ 2 };               // Current view scale
	
	bool			m_bChangedTime{ true };		// True when the time has just changed
	int				m_SliderTime{ 840 };		// Time in minutes received from slider, 14:00 by default
	ActiveAircrafts m_ActiveAircrafts;			// Contains all active aircrafts at  m_SliderTime
	TFXDataTip		m_AircraftInfo;				// Contains information about an active aircraft 
	
	bool			m_EditCoordinates{ false };	// True if program is in the edit coordinates mode
	std::string		m_CoordinateArea;			// Name of the coordinate area as given by the user
	CPoint			m_SelectedPoint;            // Point selected to save the coordinates of
	std::stringstream m_Coordinates;			// Output for the coordinate file
	int				m_NrOfCoordinates{ 0 };		// Saves the number of clicked coordinates
};

#ifndef _DEBUG  // debug version in GatorgraphicsView.cpp
inline CGatorgraphicsDoc* CGatorgraphicsView::GetDocument() const
   { return reinterpret_cast<CGatorgraphicsDoc*>(m_pDocument); }
#endif

