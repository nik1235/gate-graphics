#pragma once
#include "afxcmn.h"


// CZoomDialog dialog

class CZoomDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CZoomDialog)

public:
	CZoomDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CZoomDialog();

// Overrides
public:
	BOOL OnInitDialog() override;

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = ID_ZOOM_DLG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	// Stores the current view scale
	int m_Scale;
protected:
	// Stores the scale selected by the spin button
	CSpinButtonCtrl m_Spin;
};
