// TimeDlg.cpp : implementation file
//
#include "afxdialogex.h"
#include "../dom/stdafx.h"

#include "../dom/Gatorgraphics.h"
#include "TimeDlg.h"
#include "../utl/my_time.h"

#include <string>

// CTimeDlg dialog

IMPLEMENT_DYNAMIC(CTimeDlg, CDialogEx)


CTimeDlg::CTimeDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_TIME_DLG, pParent)
	//, m_TimeString(_T(""))
{

}

CTimeDlg::~CTimeDlg()
{
}

void CTimeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_V_SLIDER_TIME_CTR, m_HSliderTimeCtr);
	
	// Time in minutes
	int minutes = _wtoi(m_HSliderTime.GetString());
	DDX_Text(pDX, IDC_V_SLIDER_TIME, minutes);
	
	// Time in as string
	std::string time = minutes_2_timestring(minutes);
	m_TimeString = time.c_str();
	DDX_Text(pDX, IDC_TIME_STRING, m_TimeString);
	DDV_MaxChars(pDX, m_TimeString, 6);
}


BEGIN_MESSAGE_MAP(CTimeDlg, CDialogEx)
	ON_WM_HSCROLL()
END_MESSAGE_MAP()


// CTimeDlg message handlers


BOOL CTimeDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_HSliderTimeCtr.SetRange(0, 1440, TRUE);
	int temp = _wtoi(m_HSliderTime.GetString());
	m_HSliderTimeCtr.SetPos(temp);
	m_HSliderTime.Format(_T("%d"), temp);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CTimeDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{


	if (pScrollBar == (CScrollBar *)&m_HSliderTimeCtr)
	{
		int value = m_HSliderTimeCtr.GetPos();
		m_HSliderTime.Format(_T("%d"), value);
		UpdateData(FALSE);
	}
	else {
		CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
	}

	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}
