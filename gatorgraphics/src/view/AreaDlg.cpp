// InsertArea.cpp : implementation file
//

#include "../dom/stdafx.h"
#include "../dom/Gatorgraphics.h"
#include "AreaDlg.h"
#include "afxdialogex.h"


// CAreaDlg dialog

IMPLEMENT_DYNAMIC(CAreaDlg, CDialogEx)

CAreaDlg::CAreaDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_AREA_DLG, pParent)
{

}

CAreaDlg::~CAreaDlg()
{
}

void CAreaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_Area);
	DDV_MaxChars(pDX, m_Area, 4);
}


BEGIN_MESSAGE_MAP(CAreaDlg, CDialogEx)
END_MESSAGE_MAP()


// CAreaDlg message handlers


BOOL CAreaDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  Add extra initialization here

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}
