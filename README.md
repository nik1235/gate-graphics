# Gator Graphics

This application generates a visualation of aircrafts on gates at Schiphol. It contains a map of Schiphol and shows which gates are occupied at a certain moment in time. The user is able to select a certain time of the day with a slide bar and the program shows which gates are occupied by drawing a red crosses through it. When the user hoovers over a red cross, the program shows information about the aircraft that is currently active at that particulair gate. Figure 1 shows the map of Schiphol, all yellow and blue aircrafts on the map are spots where it is possible to park an aircraft, confirm with the business we will call them "VOP" [Vliegtuig Opstel Plaats]. 

![Alt text](https://bytebucket.org/nik1235/gate-graphics/raw/610e9bbf4b9494fa7223e8fa6936c6aecc3247de/doc/sketch_gates.jpg )
Figure 1: Map of Schiphol

The program shows the information for one particular day, which day this is depends on the input file: `FinalSolution.txt`. The program uses the following input and output files: 

## Input

All input files are space seperated files.


- `FinalSolution.txt`: Contains a list of all aircrafts leaving on one day at the airport Schiphol. It indicates how long a VOP is occupied by the aircraft and it contains extra information about the aircraft as the type and the arriving and departing countries, stations and flightnumbers. 
- `Vops_input.txt:` A file with VOPs that are used at this day. It contains extra information about the VOP: what is the size, what is the type: Buffer, Change, Flexible, Shengen, Non-Shengen. 
- `Vops_skipped.txt` : Contains the VOPs that are not used on this day, it has the same information per VOP as the previous file. 
- `Coordinates.txt` : A file which indicates the coordinates per VOP in the output. 

## Features 

The program gives an overview which VOP is occupied on a certain time of the day. 

* The user is be able to select a certain time for which he is able to see which gates are occupied. The default time is 14:00 and the currently selected time is displayed in the bottom left corner of the status bar. The user can change the time by clicking the clock in the toolbar or on `edit - time` in the menu. This shows a slide bar with on the left the time in minutes after midnight and on the right the time in hours::minutes. 
* VOPs that are not free are displayed as an aircraft with a red X. When the user hoovers over an occupied VOP the program shows a blue tooltip with information about the aircraft. 
* VOPs that are free are displayed as an aircraft without a red X. 

* The output must be compatible on different screen sizes. The user can select the size of the output by clicking the magnifier in the toolbar or on  `view - zoom` in the menu.

* The program contains a map of Schiphol in the future and as the airport is trying to expand the coordinates of the VOPs are not constant. Therefore the user must be able to change the coordinates in 
`Coordinate.txt`. To be able to do this without guessing, there is a Coordinate mode which can be enabled by clicking on the XY button in the toolbar or on the `edit - coordinates` in the menu. First the user 
is asked to give the name of the Vop group (also called area), these are 3 or 1 character long codes and are also displayed on the map, for example CG1. This code will be printed to the `output.txt` 
file. If the user clicks on the screen it shows a purple cross and the coordinates of this point are written to the file. If the user clicks on all VOPs of for example CG1 and then closes 
the coordinate mode (by again clicking on the XY button or the `edit - coordinate` button), all the coordinates are saved in the output.txt file. If the user want to use these coordinates, he 
can copy this output to the input file `Coordinates.txt`. In this file it is also possible to give the VOP a different name and size. 

![Alt text](https://bytebucket.org/nik1235/gate-graphics/raw/7bbb333cfd7e659b42c8705db7248c12c7e1911c/gatorgraphics/res/toolbar1.bmp)
Figure 2: The toolbar, the icons from left to right indicate: Coordinate mode, Zoom In/Zoom out and Change Time. 

All the data is read and processed in C++, the GUI is supported by MFC, all coding is done in Visual Studio 2015.  
