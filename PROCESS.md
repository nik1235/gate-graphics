Process Book
===========
# day 1
- Decide which features should present
- Find background literature 
- Write proposal
# day 2
- Study available data, decide which information to use 
- First thoughts about structure as in the following picture

![Alt text](https://bytebucket.org/nik1235/gate-graphics/raw/1cee08af06be7ebee636713b017d52a2a42d92d0/doc/sketch_structure.jpg)

# day 3
- Define classes and draw links between them
- Initialize C++ project, add source files 
# day 4
- Further research on data, start read solution file
- Start creation objects
# day 5
- Finished reading data and creation objects (not tested yet)
- Start Reading MFC for visualisation
- Finished Design Document

# day 6
- Read Chapter 13 and part 14 from Visual C++, created a sample simple Sketcher application (SDI)
- Start creation sample Paint application (MDI)
# day 7
- Read Chapter 14 and 15 from Visual C++, worked further on the Paint a sample application
- Start own MFC project, decided to go for a SDI project
- Tried to load a background bitmap picture
# day 8
- Able to load a background picture
- Implemented a scrollview (also used in the sample Paint application)
- Created a view button, where the user can rescale the application
# day 9
- Start with saving coordinates, create a button and a mode which activates saving the coordinates
- Encountered problems in saving the coordinates, coordinate system differs from what the user sees, likely
because of the scrollview. Found a convert to logical coordinates. 
- Able to draw a cross on a point where the user clicks (implemented a OnMouseClick)
# day 10
- Saves the coordinates as text and writes them to a file
- Start implementation time slide bar, created a button and a dialog

# day 11
- Added the earlier created objects (airport - vopgroups - vops - aircrafts); which read all the files
- Created a link between objects and View via the `GatorGraphDoc` class
# day 12
- Connect Time slidebar is connected with application. Application is able to show crosses for which VOPS are occupied at a certain moment in time
# day 13
- Fixed bugs in reading the data: some information was added to copy of the objects not to the objects themselves, which causes
information to be last later
- Don't use references as members for the objects, the variables to which the members reference are destroyed after the function ends.
# day 14
- Created a check mode to the coordinate button which enables the user to see whether he is in coordinate mode
- Created an area dialog : User can choose an area for which he wants to save the coordinates, created a dialog box for this and the
name of this area is written to the file (output.txt)
# day 15
- Added Area dialog to enable the user to select an area for saving the coordinates in output.txt
- Added all coordinates to the input file `Coordinates.txt`

# day 16
- Start implementation tooltip [Show information on hoover over]
- Created a mouse over function and an active aircraft vector which saves all the aircraft active at this moment
- Shows a cross when someone hoovers over a VOP
# day 17
- Used a rectangle and `TEXTOUT` to right information to show the information. 
- Removed the rectangle with invalidate rectangle. Does not look pretty: look for other implementation.
# day 18
- Finished implementation tooltip, used classes (`TFXDataTip`) from different source
- Updated the classes so that they work for this application
- Improved const correctness, names of variables
# day 19
- Show time in statusbar
- Finished different setup program: view/domain/utilities: removed the `Occupied` class: every aircraft
in the input files are unique, no need for this extra class. Renamed gator_graph to airport to make a more clear 
distinction between MFC and C++ classes. 
- Error handling with warning messages. Checks whether the program can open the input files
# day 20
- Improved comments
- Finished README.md
- Finished REPORT.md